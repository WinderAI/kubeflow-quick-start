# Kubeflow Quick Start

This is a temporary holding ground for my tests with kubeflow.

All of this was developed on OSX and assumes you have a google cloud account that is capable of using GKE. The `gcloud` CLI is also required.

## Usage

All of the settings used in the scripts are located in `env.sh`. Change this to alter the setup.

You may need to add a `GITHUB_TOKEN` variable so that you don't max out the GH API.

### Typical workflows

The scripts implement a part of a typical workflow:

- Create a cluster on google cloud: `./create-cluster.sh`
- Run kubeflow on the cluster: `./create-kubeflow.sh`
- Forward jupyter hub: `./forward-jupyter.sh` (see some example code [here](https://github.com/tensorflow/tensorflow/blob/r1.4/tensorflow/examples/tutorials/mnist/mnist_softmax.py))
- Serve an inception TF model: `./serve-inception.sh`
- Run a TF training job: `./create-training-job.sh`

Note that I could not get the following working within the time restraints:

- Run inception client: `./run-inception-client.sh`
- Forward tensorboard: `./forward-tensorboard.sh`

### Destruction

Run the `./destroy.sh` script to delete the cluster and it's components.