#!/usr/bin/env bash

set -ex

source env.sh

echo "THIS DOES NOT WORK"

if ! ls serving ; then
    git clone --recursive https://github.com/tensorflow/serving
fi

if ! ls .venv ; then
    virtualenv -p python2.7 .venv
fi
source .venv/bin/activate

pip install --upgrade pip
pip install tensorflow tensorflow-serving-api

wget https://upload.wikimedia.org/wikipedia/en/a/ac/Xiang_Xiang_panda.jpg

python3 serving/tensorflow_serving/example/inception_client.py --server=localhost:9000 --image=./Xiang_Xiang_panda.jpg