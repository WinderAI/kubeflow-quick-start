#!/usr/bin/env bash

set -ex

source env.sh

# RBAC: Create cluster role for your user
CLUSTERROLEBINDING_NAME=default-admin
if ! kubectl get clusterrolebinding | grep ${CLUSTERROLEBINDING_NAME} ; then
    CURRENT_USER=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
    kubectl create clusterrolebinding ${CLUSTERROLEBINDING_NAME} --clusterrole=cluster-admin --user=${CURRENT_USER}
else
    echo "clusterrolebinding ${CLUSTERROLEBINDING_NAME} already exists"
fi

# From https://github.com/kubeflow/kubeflow

# Create a namespace for kubeflow deployment
if ! kubectl get namespace | grep ${NAMESPACE} ; then
    kubectl create namespace ${NAMESPACE}
else
    echo "namespace ${NAMESPACE} already exists"
fi

# Initialize a ksonnet app. Set the namespace for it's default environment.
if ! ls ${APP_NAME} ; then
    ks init ${APP_NAME}

    cd ${APP_NAME}

    ks env set default --namespace ${NAMESPACE}

    # Install Kubeflow components
    ks registry add kubeflow github.com/kubeflow/kubeflow/tree/${VERSION}/kubeflow
    ks pkg install kubeflow/core@${VERSION}
    ks pkg install kubeflow/tf-serving@${VERSION}
    ks pkg install kubeflow/tf-job@${VERSION}

    # From https://github.com/kubeflow/kubeflow/blob/master/user_guide.md#minikube

    # Create templates for core components
    ks generate core kubeflow-core --name=kubeflow-core

    ks env add cloud
    ks param set kubeflow-core cloud gke --env=cloud

    # Enable collection of anonymous usage metrics
    # Skip this step if you don't want to enable collection.
    ks param set kubeflow-core reportUsage true
    ks param set kubeflow-core usageId $(uuidgen)

    # Add persistent disk for jupyter notebooks
    ks param set kubeflow-core jupyterNotebookPVCMount /home/jovyan

    # Tell kubeflow to use the following namespace for this environment
    ks env set ${KF_ENV} --namespace ${NAMESPACE}

else
    cd ${APP_NAME}
    echo "${APP_NAME} already exists"
fi

# Show config
ks show ${KF_ENV} -c kubeflow-core

# Create!
ks apply ${KF_ENV} -c kubeflow-core