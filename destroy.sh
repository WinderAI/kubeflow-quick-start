#!/usr/bin/env bash

set -ex

source env.sh

gcloud container clusters delete ${CLUSTER_NAME} --zone=${ZONE}

gcloud -q compute firewall-rules delete $(gcloud compute firewall-rules list --uri) || true
gcloud -q compute forwarding-rules delete $(gcloud compute forwarding-rules list --uri) || true
gcloud -q compute disks delete $(gcloud compute disks list --uri) || true
gcloud -q compute target-pools delete $(gcloud compute target-pools list --uri) || true
gcloud -q compute http-health-checks delete $(gcloud compute http-health-checks list --uri) || true
