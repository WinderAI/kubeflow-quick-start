#!/usr/bin/env bash

set -ex

source env.sh

PODNAME=$(kubectl get pods --namespace=${NAMESPACE} --selector="app=inception" --output=template --template="{{with index .items 0}}{{.metadata.name}}{{end}}")
kubectl exec --namespace=${NAMESPACE} -it ${PODNAME} -- /bin/bash
